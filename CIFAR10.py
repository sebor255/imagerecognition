import numpy
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, BatchNormalization, Activation
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.constraints import maxnorm
from keras.utils import np_utils

# Set random seed for purposes of reproducibility
seed = 21

from keras.datasets import cifar10

# loading in the data


(X_train, y_train), (X_test, y_test) = cifar10.load_data()


# normalize the inputs from 0-255 to between 0 and 1 by dividing by 255

X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train = X_train / 255.0
X_test = X_test / 255.0

# Show example image:
# from PIL import Image
# a = Image.fromarray(X_train[0])
# a.show()

# one hot encode outputs
y_train = np_utils.to_categorical(y_train)
y_test = np_utils.to_categorical(y_test)
class_num = y_test.shape[1]


### DESIGNING THE MODEL

model = Sequential()

# model paramters
channels = filters = 32
filter_size = (3, 3)
padding = 'same'                    # we do not change the size of the image
input_shape = X_train.shape[1:]
activation_type = 'relu'            # most common activation

model.add(Conv2D(filters,filter_size, input_shape=input_shape, padding=padding))
model.add(Activation(activation_type))

# making a dropout layer
# to prevent overfitting - it randomly eliminates some of the connections between layers

model.add(Dropout(0.2))     # means 20% is dropped

# Batch Normalization normalizes the inputs heading into the next layer,
# ensuring that the network always creates activations with the same distribution that we desire:
model.add(BatchNormalization())

# second convultional layer
# Now comes another convolutional layer, but the filter size increases so the network
# can learn more complex representations:

model.add(Conv2D(64, filter_size, padding=padding))
model.add(Activation(activation_type))

# Polling Layer
# Here's the pooling layer, as discussed before this helps make the image classifier more robust
# so it can learn relevant patterns. There's also the dropout and batch normalization:

model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.2))
model.add(BatchNormalization())


# adding next convultional layer
model.add(Conv2D(64, (3, 3), padding='same'))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.2))
model.add(BatchNormalization())

# adding next convultional layer
model.add(Conv2D(128, (3, 3), padding='same'))
model.add(Activation('relu'))
model.add(Dropout(0.2))
model.add(BatchNormalization())

# flattening the data
model.add(Flatten())
model.add(Dropout(0.2))

# creating densely connected layer
# We need to specify the number of neurons in the dense layer. Note that the numbers of neurons in succeeding
# layers decreases, eventually approaching the same number of neurons as there are classes in the dataset
# (in this case 10). The kernel constraint can regularize the data as it learns,
# another thing that helps prevent overfitting. This is why we imported maxnorm earlier.

model.add(Dense(256, kernel_constraint=maxnorm(3)))
model.add(Activation('relu'))
model.add(Dropout(0.2))
model.add(BatchNormalization())

model.add(Dense(128, kernel_constraint=maxnorm(3)))
model.add(Activation('relu'))
model.add(Dropout(0.2))
model.add(BatchNormalization())

# Finally, the softmax activation function selects the neuron with the highest probability as its output,
# voting that the image belongs to that class:

model.add(Dense(class_num))
model.add(Activation('softmax'))


### MODEL HAS BEEN DESIGNED
# Now that we've designed the model we want to use, we just have to compile it.
# Let's specify the number of epochs we want to train for, as well as the optimizer we want to use.
# The optimizer is what will tune the weights in your network to approach the point of lowest loss.
# The Adam algorithm is one of the most commonly used optimizers because it gives great performance on most problems:
epochs = 25
optimizer = 'adam'

# Let's now compile the model with our chosen parameters. Let's also specify a metric to use.
model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])

print(model.summary())

# Now we get to training the model. To do this, all we have to do is call the fit() function on the model
# and pass in the chosen parameters.

numpy.random.seed(seed)
model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=epochs, batch_size=64)


# Now we can evaluate the model and see how it performed. Just call model.evaluate():
# Model evaluation
scores = model.evaluate(X_test, y_test, verbose=0)
print("Accuracy: %.2f%%" % (scores[1]*100))