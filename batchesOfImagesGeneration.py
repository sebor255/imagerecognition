import os
os.environ['KERAS_BACKEND'] = 'tensorflow'
from keras.preprocessing.image import ImageDataGenerator

gen = ImageDataGenerator()

train_generator = gen.flow_from_directory(
        '.\\',  # this is the target directory
        target_size=(100, 100),  # all images will be resized
        color_mode='rgb',
        classes=None,
        batch_size=64,
        shuffle=True,
        seed=None,
        save_to_dir=None,
        save_prefix='',
        class_mode='categorical')
        
for image_batch,labels in train_generator:
        print(image_batch, labels)