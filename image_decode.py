import tensorflow                        # tensorflow 2.0


def decode_image(filename, image_type, resize_shape, channels):
    value = tensorflow.io.read_file(filename)
    if image_type == 'png':
        decoded_image = tensorflow.image.decode_png(value, channels=channels)
    elif image_type == 'jpeg':
        decoded_image = tensorflow.image.decode_jpeg(value, channels=channels)
    else:
        decoded_image = tensorflow.image.decode_image(value, channels=channels)

    if resize_shape is not None and image_type in ['png', 'jpeg']:
        decoded_image = tensorflow.image.resize(decoded_image, resize_shape)

    return decoded_image



# The output of the map function is a new dataset with each element now converted from the original image file
# to its corresponding pixel data. We use map rather than using a for loop to manually convert each image file
# because map does the image decoding in parallel across the files, making it a more efficient solution.
def get_dataset(image_paths, image_type, resize_shape, channels):
    filename_tensor = tensorflow.constant(image_paths)
    dataset = tensorflow.data.Dataset.from_tensor_slices(filename_tensor)

    def _map_fn(filename):
        decode_images = decode_image(filename, image_type, resize_shape, channels=channels)
        return decode_images

    map_dataset = dataset.map(_map_fn)  # we use the map method: allow to apply the function _map_fn to all the
    # elements of dataset
    return map_dataset


def get_image_data(image_paths, image_type, resize_shape, channels):
    dataset = get_dataset(image_paths, image_type, resize_shape, channels)
    iterator = tensorflow.compat.v1.data.make_one_shot_iterator(dataset)
    next_image = iterator.get_next()

    return next_image


# main

picturePath = r"C:\Users\AMD\Desktop\Python\Studia\Uczenie Maszynowe\imagerecognition\myBalls\balls.png"

values = tensorflow.io.read_file(picturePath)

image = get_image_data([picturePath], 'png', None, 0)
print(image)

# import numpy as np
# a = np.asarray(image)
# a = Image.fromarray(a)
# a.show()