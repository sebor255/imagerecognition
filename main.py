import tensorflow as tf                         # tensorflow 2.0
import keras
from keras.datasets import mnist
import numpy as np

seed = 0
np.random.seed(seed)                        # fix random seed
tf.random.set_seed(seed)

# input image dimensions
num_classes = 10                            # 10 digits

img_rows, img_cols = 28, 28                 # number of pixels

# the data, shuffled and split between train and test sets
(X_train, Y_train), (X_test, Y_test) = mnist.load_data()
print(X_train)

# transforming data to NHWC format:
#The default value for channels is 0, which means the decoding function uses the interpretation specified from the
# raw data. Setting channels to 1 specifies a grayscale image, while setting channels to 3 specifies an RGB image.
# For PNG images we're also allowed to set channels to 4, corresponding to RGBA images. Setting channels to 2
# is invalid. (channel is the last parameter in below reshape() method)

X_train = X_train.reshape(X_train.shape[0], img_rows, img_cols, 1)
X_test = X_test.reshape(X_test.shape[0], img_rows, img_cols, 1)
input_shape = (img_rows, img_cols, 1)
print(X_train)

# cast floats to single precision
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
print(X_train)

# rescale data in interval [0,1]
X_train /= 255
X_test /= 255
print(X_train)


# We need to transform our classes into vectors.
print(Y_train)
Y_train = keras.utils.to_categorical(Y_train, num_classes)
Y_test = keras.utils.to_categorical(Y_test, num_classes)
print(Y_train)